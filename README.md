# Miscellaneous Scripts

| Sr. No. | Name | Description | Platform |
|---|---|---|---|
| 1 | FirefoxProfiles | command to start each firefox profile | Linux |

# Credits
App Icon by Hea Poh Lin, from The Noun Project.
