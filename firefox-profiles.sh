#
# This is free and unencumbered software released into the public domain.

# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.

# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>
#
#------------------------------------------------------------------------------------------------
#
# Bash Script to launch different firefox profiles from command line.
#
# How to use
#
# Replace p2 and p3 with the argument name you want to provide to this script.
# These arguments will identify profile.
#
# Replace SECONDPROFILE and THIRDPROFILE string with your actual firefox profile names.
# Profile names are cASe SenSITIve and should exactly match your firefox profile names!
#
# Replace URL with actual url you want that firefox profile to load on start.
#
# Rename script to any short name you prefer and easy to remember.
#
# After all replacing and script renaming is done,
# create a symlink to this script OR directly copy this script at /usr/bin/
# OR the preferred bin directory of your linux distribution.
#
# Mark script as executable and should be owned by root.
#
#
#-----------------------------------------------------------------------------------------------
#
#!/bin/bash

case "$1" in
    def)
        ( firefox -no-remote -P default & disown) &>/dev/null </dev/null & disown
        ;;
    p2)
        ( firefox -no-remote -P SECONDPROFILE "URL" & disown ) &>/dev/null </dev/null & disown
        ;;
    p3)
        ( firefox -no-remote -P THIRDPROFILE & disown ) &>/dev/null </dev/null & disown
        ;;
    *)
        echo $"USage: $0 {def | p2 | p3}"
        exit 1
esac
